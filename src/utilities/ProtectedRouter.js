import React, {useContext} from "react";
import { Route, Redirect } from "react-router-dom";
import { ContextApp } from "../containers/App";

export default ({ children, redirect = "/login", ...rest }) => {
    const contextApp = useContext(ContextApp);

    return (

        <Route {...rest}>
            { console.log("rest.path", rest.path)}
            {contextApp.isLogin ? children : <Redirect to={redirect} />}
        </Route>
    );
};