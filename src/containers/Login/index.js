import React, {useContext, useState}  from "react";
import { useHistory, Redirect } from "react-router-dom";
import {ContextApp} from "../App";
import {login} from "../../utilities/apiLogin";
import { Wrapper } from "./styles";

export default () => {
    const history = useHistory();
    const contextApp = useContext(ContextApp);
    const [username, setUser] = useState("");
    const [password, setPass] = useState("");

    const handleSubmit = (e) =>{
        e.preventDefault();
        login({username,password}, () => {contextApp.onIsLogin(true); history.push("/")});
    }

    if (contextApp.isLogin) return <Redirect to="/" />;

    return (

        <Wrapper>
            <h1>Login</h1>
            <form onSubmit={handleSubmit}>
                <div>
                    <label htmlFor="username" >Usuario</label>
                    <input  id="username" type="text" name="username" value={username} onChange={e => setUser(e.target.value)}/>
                </div>
                <div>
                    <label htmlFor="password" >Contraseña</label>
                    <input  id="password" type="password" name="password" value={password} onChange={e => setPass(e.target.value)}/>
                </div>
                <button>Inisiar Sesion</button>
            </form>
        </Wrapper>
    );
};
