import React, {useState, createContext }  from "react";
import { Switch, Route } from "react-router-dom";
import Login from "../Login";
import validarSesion from "../../utilities/validarSesion";
import ProtectedRouter from "../../utilities/ProtectedRouter";
import Container from "../../components/Container";
import Formulario from "../../components/Formulario";

/**
 * Para el punto 9, utilizo el context que ya habia definido
 */
export const ContextApp = createContext({});

localStorage.setItem("respCorrecta", 0);

const contenidoFormulario = [{
    "pregunta" : "la suma de 2 mas 2 es:" ,
    "opciones" : [
        {
            "nombreOpcion" : "4" ,
            "esVerdadera" : true
        },
        {
            "nombreOpcion" : "3" ,
            "esVerdadera" : false
        }
    ]
},
    {
        "pregunta" : "la multiplicacion de 2 *2:" ,
        "opciones" : [
            {
                "nombreOpcion" : "4" ,
                "esVerdadera" : true
            },
            {
                "nombreOpcion" : "3" ,
                "esVerdadera" : false
            }
        ]
    },
    {
        "pregunta" : "la division de 2 / 2 es:" ,
        "opciones" : [
            {
                "nombreOpcion" : "1" ,
                "esVerdadera" : true
            },
            {
                "nombreOpcion" : "0" ,
                "esVerdadera" : false
            }
        ]
    },
    {
        "pregunta" : "la resta de 2 - 2 es:" ,
        "opciones" : [
            {
                "nombreOpcion" : "0" ,
                "esVerdadera" : true
            },
            {
                "nombreOpcion" : "1" ,
                "esVerdadera" : false
            }
        ]
    },
    {
        "pregunta" : "2^2 es:" ,
        "opciones" : [
            {
                "nombreOpcion" : "4" ,
                "esVerdadera" : true
            },
            {
                "nombreOpcion" : "3" ,
                "esVerdadera" : false
            }
        ]
    }
    ];

const Resultado = () => {
   return(
    <div>
        <h3>Resultados</h3>
        {        <label>De  {contenidoFormulario.length} preguntas en la encuesta, usted contesto {localStorage.getItem("respCorrecta")&&localStorage.getItem("respCorrecta")} correctamente </label>}
    </div>
   );
};
const App = () => {
    const sesion = validarSesion()
    const [isLogin, setIsLogin] = useState(sesion);
    const [next, setNext] = useState(0);
    const [isEncuenta, setIsEncuenta] = useState(false);
    const [cont, setCont] = useState(0);
    const onSetCont = (val) => setCont(val);

    const onIsLogin = val => {setIsLogin(val)};

    const onNext = val => {setNext(val)}

    const onEncuesta = val => {setIsEncuenta(val)}

    return (
        <ContextApp.Provider value={{ onIsLogin , isLogin, onEncuesta, isEncuenta, onSetCont, cont  }}>
        <Switch>
            <Route path="/resultado" component={Resultado} />

           <ProtectedRouter  exact path="/" >
               {
                   <Container key={next} title={contenidoFormulario[next].pregunta}>
                        <Formulario id={next} options={contenidoFormulario[next].opciones} onChange={onNext} len={contenidoFormulario.length}/>
                   </Container>
               }
           </ProtectedRouter>
            <Route path="/login" component={Login} />

        </Switch>
        </ContextApp.Provider>
    );
}

export default App;