import React, {useContext, useState} from 'react'
import { useHistory } from "react-router-dom";
import Select from "react-select";
import { Formik, Form, Field } from "formik";
import {ContextApp} from "../../containers/App";

const Formulario = ({id, options, onChange, len}) =>{
    const data = options.map(opt => ({
        label: opt.nombreOpcion,
        value: opt.esVerdadera
    }));

    const history = useHistory();
    const contextApp = useContext(ContextApp);

    const  SelectField = (props) => {
        return (
            <Select
                options={props.options}
                {...props.field}
                onChange={option => {
                    props.form.setFieldValue(props.field.name, option);
                    option.value&&localStorage.setItem("respCorrecta", parseInt(localStorage.getItem("respCorrecta")) + parseInt(1));
                }}
            />
        )
    }



    return (
        <Formik initialValues={{data, select: ""}}
                validate={values => (!values.select&&alert("Debe seleccionar una opción"))}
                onSubmit={() => {

                    if (parseInt(id)+1!==len) {
                        onChange(parseInt(id) + 1);
                    }
                    else {
                        contextApp.onEncuesta(true);
                        history.push("/resultado");
                    }
                }
                }
        >
            {({values, handleSubmit}) =>
                <Form onSubmit={handleSubmit}>
                    <Field name="select" options={values.data} component={SelectField}/>
                    {(parseInt(id)+1)===len?<button>Finalizar</button>:<button>Siguiente</button>}
                </Form>
            }
        </Formik>

    );
};

export default Formulario;